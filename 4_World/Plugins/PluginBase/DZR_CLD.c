class CLDDiscordMessage3
{
	string username;
	string content;
	string avatar;
}

class DZR_CLD extends PluginBase
{
    
    ref array<string> m_NullArray = new array<string>;
    string m_StoreFolder = "$profile:/DZR/CombatLogDetection/Prisoners";
    
	dzr_cld_config_data_class	m_CLDexistingConfig;
	private PlayerBase 						m_Source;
	private BleedingSourcesManagerServer 	m_BleedMgr;	
	
	private string 		PlayerName;
	private string 		Victim;
	private string 		Killer;
	private string 		Message;
	private string 		VictimId;
	private string		KillerId;
	private string		KillWeapon;	
	private string 		KillMessage;
	private string 		DiscordMessage;
	private string 		DeathDetails;			
	private float 		m_Distance;
	private int 		kill_Distance;
	private int 		player_StatWater;
	private int 		player_StatEnergy;
	private string 		killTime;
	
	private int 		prevCoord;
	
	ref map<PlayerBase, bool> m_WaitingForPenalty = new map<PlayerBase, bool>;
	
	private bool StopDiscordMessage = true;
	private bool StopSideChatMessage = true;
	
	const int Player_Food 	 = 	200 ,Player_Water	 = 	300, Player_Blood	 =	3500 ;
	
	int year, month, day, hour, minute, second;
	string sYear, sMonth, sDay, sHour, sMinute, sSecond, currentTime, currentDate;	
	
	
	void DZR_CLD( dzr_cld_config_data_class externalConfig)
	{		
		
		prevCoord = 0;
		
		if (GetGame().IsMultiplayer())
		{
			m_CLDexistingConfig = GetDayZGame().ModConfig("DZR_CLD.c");
			//Print("[DZR_CLD] :: Init");
        }	
    }
	
	
	void ~DZR_CLD()
	{
		if (GetGame().IsMultiplayer())
		{
			Print("[DZR_CLD] :: Closed");
        }
    }		
	
	string ReturnUtcTime()
	{			
		GetYearMonthDayUTC(year, month, day);
		GetHourMinuteSecondUTC(hour, minute, second);				
		sYear = year.ToString();
		sMonth = month.ToString();
		if (sMonth.Length()  == 1)		{			sMonth  = "0"   + sMonth;		}				sDay    = day.ToString();
		if (sDay.Length()	 == 1)		{			sDay    = "0"   + sDay;			}				sHour   = hour.ToString();
		if (sHour.Length()   == 1)		{			sHour   = "0"   + sHour;		}				sMinute = minute.ToString();
		if (sMinute.Length() == 1)		{			sMinute = "0"   + sMinute;		}				sSecond = second.ToString();
		if (sSecond.Length() == 1)		{			sSecond = "0"   + sSecond;		}						
		currentDate = 		sYear + "-" + sMonth  + "-" + sDay;
		currentTime = "T" + sHour + ":" + sMinute + ":" + sSecond + "Z";
		return currentDate + currentTime;
    }
	
	protected string GetPrefix(PlayerIdentity identity)  // Get player names
	{
		PlayerName = "Survivor (AI)";
		if (identity)
		{
			PlayerName = identity.GetName();
        }
		identity = NULL;		
		return PlayerName;
    }	
	
	void PlayerHit(PlayerBase player, EntityAI source)
	{
        
        // NotificationSystem.SendNotificationToPlayerIdentityExtended( player.GetIdentity() , 5, "Damage!", source.GetName(), "set:ccgui_enforce image:MapShieldBooster");
        // Print(source);
        // Print(player);
         
        if( ( source.IsZombie() || source.IsAnimal() ) && m_CLDexistingConfig.PlayerOnlyDamage)
        {
            Print("Not a player. Ignore.");
            return;
        }
        
        if(player == PlayerBase.Cast(source))
        {
            
            //       Print("Self Damage. Ignored.");
            
            return;
        }
        
        if(player == PlayerBase.Cast(source))
        {
            
            //       Print("Self Damage. Ignored.");
            
            return;
        }
		m_CLDexistingConfig = GetDayZGame().ModConfig("DZR_CLD.c");
		if(m_CLDexistingConfig.AnyDamageTrigger)
		{
            
			if(source && player != PlayerBase.Cast(source) )
            {
                PlayerBase theAttacker = PlayerBase.Cast(source.GetHierarchyRootPlayer());
                if(theAttacker && theAttacker.IsPlayer() )
                {
                    if(theAttacker.IsPlayer())
                    {
                        theAttacker.setCombatMode(true, "EEHitBy");
                        theAttacker.resetCombatTimer();

                        
                    }
                }
            }
            
            PlayerBase theVictim = player; //PlayerBase.Cast(GetHierarchyRootPlayer());
            
            //this.Debug("Player "+theVictim.GetIdentity().GetName() +" is hit by "+source);
            if(theVictim)
            {
                if(theVictim.IsPlayer() && theVictim.GetIdentity())
                {
                    theVictim.setCombatMode(true);
                    theVictim.resetCombatTimer();
                    //string DebugMsg = the_killer.GetIdentity().GetName() +" killed other player and is in Combat Mode.";
                    //this.Debug(DebugMsg);
                }
                
            }            
        }
    }
    void PlayerKilled(PlayerBase player, Object source)
    {
        
        Print(player);
        Print(player.GetIdentity());
        
        if (player && source && player.GetIdentity() != NULL)
        {												
            Print("[DZR_CLD] :: Player died and source was: " + source.GetDisplayName());
            Victim = this.GetPrefix( player.GetIdentity() );
            
            if( player.GetIdentity() != NULL && player.IsPlayer() )
            {
                VictimId = player.GetIdentity().GetPlainId();
                DeleteFile(m_StoreFolder+"/"+VictimId+".txt");
            }
            KillMessage = "";
            StopSideChatMessage  = true;
            StopDiscordMessage   = true;
            
            if (player == source) // Player killed self, bled to death, starved, etc.
            {
                //Print("[DZR_CLD] :: 140");
                player_StatWater = Math.Round(player.GetStatWater().Get());
                player_StatEnergy = Math.Round(player.GetStatEnergy().Get());
                m_BleedMgr = player.GetBleedingManagerServer();
                
                
                StopDiscordMessage = false;
                StopSideChatMessage = false;
                if ( player_StatWater && player_StatEnergy && m_BleedMgr )
                {
                    KillMessage=  Victim + " " + "Died";// Water: " + player_StatWater.ToString() + " Energy: " + player_StatEnergy.ToString() + " Bleed sources: " + m_BleedMgr.GetBleedingSourcesCount().ToString();
                }
                else if ( player_StatWater && player_StatEnergy && !m_BleedMgr )
                {
                    KillMessage= Victim + " " + "Died";// Water: " + player_StatWater.ToString() + " Energy: " + player_StatEnergy.ToString();
                }
                else
                {
                    KillMessage = Victim + " " + Victim + "UnknowMessages";
                }							
                
                DiscordMessage+= "Suicided" + "\"";
                DeathDetails = Victim + " " + "SelfDeath";					
                Print("[DZR_CLD] :: " + KillMessage);
                
            }
            else if ( source.IsWeapon() || source.IsMeleeWeapon() || EntityAI.Cast( source ).IsPlayer() )  // player with item
            {		
                Print("[DZR_CLD] :: 196");
                
                if(EntityAI.Cast( source ).IsPlayer()) // fists
                {
                    m_Source = PlayerBase.Cast( EntityAI.Cast( source ) );
                }
                else
                {
                    m_Source = PlayerBase.Cast( EntityAI.Cast( source ).GetHierarchyParent() );
                }
                
                
                Print("[DZR_CLD] :: 198");
                Killer = "";
                StopDiscordMessage = false;
                StopSideChatMessage = false;
                Print("[DZR_CLD] :: 202");
                if (m_Source)
                {
                    Print("[DZR_CLD] :: 204");
                    Killer = this.GetPrefix( m_Source.GetIdentity() );
                    KillerId = m_Source.GetIdentity().GetPlainId();
                    KillWeapon = source.GetDisplayName();
                    
                    
                    DiscordMessage+= "" + Killer + "\"";
                    Print("[DZR_CLD] :: 212");
                }				
                if ( source.IsMeleeWeapon() )
                {	
                    Print("[DZR_CLD] :: 216");
                    DeathDetails = "WBTDW" + KillWeapon;
                    
                    KillMessage = Victim + "KilledBy" + Killer + "With"+ " " + KillWeapon;		
                    Print("[DZR_CLD] :: 220");
                }
                else
                {
                    Print("[DZR_CLD] :: 224");
                    m_Distance = vector.Distance( player.GetPosition(), m_Source.GetPosition() );
                    kill_Distance = Math.Round(m_Distance);
                    DeathDetails = "Weapon"+ " " + KillWeapon + " " + "Distance" + " " + kill_Distance + "M";
                    KillMessage = Victim + "KilledBy" + Killer + "With" + KillWeapon + "From" + kill_Distance + "Meters";
                    Print("[DZR_CLD] :: 229");
                }
                Print("[DZR_CLD] :: 231");
                m_CLDexistingConfig = GetDayZGame().ModConfig("DZR_CLD.c");
                if(m_CLDexistingConfig.KillInstantPenalty)
                {
                    Print("[DZR_CLD] :: 235");
                    string warningTitle = "#STR_KILL_WARNING";
                    string warningText = "#STR_KILL_WARNING_TEXT";
                    
                    if(m_CLDexistingConfig.KillWarningTitle != "")
                    {
                        warningTitle = m_CLDexistingConfig.KillWarningTitle;
                    };
                    if(m_CLDexistingConfig.KillWarningText != "")
                    {
                        warningText = m_CLDexistingConfig.KillWarningText;
                    };
                    Print("[DZR_CLD] :: 247");
                    this.DelayedPenalties(m_Source.GetIdentity(), m_Source, false, m_Source.GetIdentity().GetName(), m_Source.GetIdentity().GetPlainId(), 0, m_CLDexistingConfig);
                    Print("[DZR_CLD] :: 249");
                    NotificationSystem.SendNotificationToPlayerIdentityExtended( m_Source.GetIdentity() , 10, warningTitle, warningText, m_CLDexistingConfig.KillWarningIcon);
                    Print("[DZR_CLD] :: 251");
                };
                
                Print("[DZR_CLD] :: " + KillMessage);
                
                if(m_CLDexistingConfig.PlayerOnlyDamage)
                {
                    return;
                };
            }
            else if (source.IsInherited(Grenade_Base)) // Player was killed by an explosion
            {
                Print("[DZR_CLD] :: 168");
                StopDiscordMessage = false;
                StopSideChatMessage = false;
                KillWeapon = source.GetDisplayName();
                DiscordMessage+= KillWeapon + "\"";
                DeathDetails = "GrenadeMessages";							
                Print("[DZR_CLD] :: " + Victim +  "GrenadeMessages");							
                
                KillMessage = Victim + KillWeapon ;									
                
            }
            else if (source.IsInherited(TrapBase)) // Player was killed by a Trap
            {
                Print("[DZR_CLD] :: 181");
                StopDiscordMessage = false;
                StopSideChatMessage = false;
                
                DeathDetails = " " + "LandMineMessages";						
                KillWeapon = source.GetDisplayName();			
                DiscordMessage+= KillWeapon + "\"";								
                Print("[DZR_CLD] :: " + Victim + "DiedToA" + KillWeapon);
                
                
                KillMessage = Victim + Victim + "DiedToA" + KillWeapon;						
                
            }
            else if (source.IsInherited(DayZInfected)) // Player was killed by an Infected
            {
                Print("[DZR_CLD] :: 250");
                StopDiscordMessage = false;
                StopSideChatMessage = false;
                Killer = source.GetDisplayName();					
                DeathDetails = Victim + " " + "ZombieKillMessages";
                DiscordMessage+=  "Zombie" + ": " + "ZombieNames" + "\"";					
                Print("[DZR_CLD] :: " + Victim + "WKB" + Killer);
                
                KillMessage = Victim + "WKB" + Killer;
                
                
            }
            else if (source.IsInherited(DayZAnimal)) // Player was killed by an animal
            {
                Print("[DZR_CLD] :: 264");
                StopDiscordMessage = false;
                StopSideChatMessage = false;
                Killer = source.GetDisplayName();								
                DiscordMessage+= "" + Killer + "\"";
                DeathDetails = "AnimalMessages";					
                Print("[DZR_CLD] :: " + Victim + "WKB" + Killer);								
                
                
            }
            else if (source.IsInherited(CarScript)) // Player was run down by vehicle
            {
                Print("[DZR_CLD] :: 276");
                StopDiscordMessage = false;
                StopSideChatMessage = false;
                DiscordMessage+= "Vehicle" + "\"";
                DeathDetails = Victim + " " + "VehicleMessages";				
                Print("[DZR_CLD] :: " + Victim + "VehicleMessages");									
                
                KillMessage = Victim + "WRDBAV";								
                
            }
            else // Player died of unknown cause
            {
                Print("[DZR_CLD] :: 288");
                StopDiscordMessage = false;
                StopSideChatMessage = false;
                DiscordMessage+= "UnknowMessages" + "\"";
                DeathDetails = "UnknowMessages";
                Print("[DZR_CLD] :: " + Victim + "UnknowMessages");
                
                
                
                KillMessage = Victim + "UnknowMessages";
                
                
            }			
            
            if (!StopSideChatMessage)
            {
                
                //BroadCastKillToChat(KillMessage);
                
            }			
            if (!StopDiscordMessage)
            {
                Print("[DZR_CLD] :: 310");
                DiscordMessage+= ",\"inline\":true},{\"name\":\"" + "Victim" +"\",\"value\":\"";									
                
                DiscordMessage+= "" + Victim + "\"";				
                
                DiscordMessage+= ",\"inline\":true},{\"name\":\"" + "KillDetails" + "\",\"value\":\"";
                DiscordMessage+= "" + DeathDetails + "\"";  // Kill details
                DiscordMessage+= "}],\"thumbnail\":{\"url\":\"";
                
                DiscordMessage+= "},\"timestamp\":\"";
                DiscordMessage+= "" + killTime +"\"}]}";
                Print(DiscordMessage);	
                DiscordMessage = "";
                
            }
            Print("[DZR_CLD] :: 325");
        }
        Print("[DZR_CLD] :: 327");
        
    }
    
    void DelayedPenalties(PlayerIdentity identity, PlayerBase player, bool authFailed, string name, string plainid, int logout, dzr_cld_config_data_class m_ExtCLDexistingConfig)
    {
        
        Print("[DZR_CLD] DelPen :: 342");
        if(!player.isInCombat())
        {
            GetDayZGame().Debug("DZR_CLD.c player not in combat, cancelling", 0, m_ExtCLDexistingConfig.EnableDebug);
            return;
        };
        
        
        
        GetDayZGame().Debug("DelayedPenalties start from DZR_CLD.c", 0, m_ExtCLDexistingConfig.EnableDebug);
        if(m_ExtCLDexistingConfig.PenaltyRemoveGear)
        {
            player.stripPlayer();
        }
        
        if( m_ExtCLDexistingConfig.PenaltyGiveItems.Count() >= 1 )
        {
            EntityAI itemCreated = null;
            for ( int item_index = 0; item_index < m_ExtCLDexistingConfig.PenaltyGiveItems.Count(); item_index++ )
            {
                itemCreated = player.GetInventory().CreateInInventory(m_ExtCLDexistingConfig.PenaltyGiveItems[item_index]);
                itemCreated.SetHealth01("", "", m_ExtCLDexistingConfig.ItemsHealth);
            }
            
        }
        
        if(m_ExtCLDexistingConfig.PenaltyBreakLegs)
        {
            //GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(player.SetBrokenLegs, 2000, false, eBrokenLegs.BROKEN_LEGS);
            player.SetBrokenLegs(eBrokenLegs.BROKEN_LEGS);
            
        };
        
        for ( int agent_index = 0; agent_index < m_ExtCLDexistingConfig.PenaltyDisieaseAgents.Count(); agent_index++ )
        {
            player.InsertAgent(m_ExtCLDexistingConfig.PenaltyDisieaseAgents[agent_index], m_ExtCLDexistingConfig.DisieaseIntensity);
        }
        
        if(m_ExtCLDexistingConfig.PenaltyTeleport)
        {
            //iterate over coords
            //select N
            //exec
            // N++
            
            // int m_IntPrisonTime_Stored;
            
            //Print(prevCoord);
            vector randCoords = m_ExtCLDexistingConfig.PenaltyTeleportCoords[prevCoord].ToVector();
            Print("TELEPORTING TO COORD "+prevCoord+": "+randCoords);
            
            player.SetPosition( randCoords );
            
            
            
            int totalCoords = m_ExtCLDexistingConfig.PenaltyTeleportCoords.Count() - 1;
            if(prevCoord >=  totalCoords)
            {
                prevCoord = 0;
            }
            else
            {
                prevCoord++;
            }
            Print("CCORDS COUNTER: "+prevCoord+" of "+totalCoords );
            //player.SetOrientation( m_ExtCLDexistingConfig.PenaltyTeleportRotate.ToVector() );
            
            if( m_ExtCLDexistingConfig.BuildingLockdown )	
            {
                LockDoors(randCoords, 50);
            }
            player.SetPrisoner(true);
            player.setCombatMode(false);
            Print("TimeToKeepInPrison_m: "+m_ExtCLDexistingConfig.TimeToKeepInPrison_m);
            if(m_ExtCLDexistingConfig.TimeToKeepInPrison_m >= 1)
            {
                Print("m_ExtCLDexistingConfig.TimeToKeepInPrison_m >= 1");
                string m_IntPrisonTime_Stored = dzr_cld_CFG_class.GetFile(m_StoreFolder, player.GetIdentity().GetPlainId()+".txt", m_NullArray, m_ExtCLDexistingConfig.TimeToKeepInPrison_m.ToString() );
                Print("Created m_IntPrisonTime_Stored file?: "+m_IntPrisonTime_Stored)
            }
            
            
        }
        
        
        
        float currentHealth = player.GetHealth("","");
        float currentBlood = player.GetHealth("","Blood");
        float currentShock = player.GetHealth("","Shock");
        player.SetHealth("", "", currentHealth - m_ExtCLDexistingConfig.PenaltyHealth);
        player.SetHealth("", "Blood", currentBlood - m_ExtCLDexistingConfig.PenaltyBlood);
        player.SetHealth("", "Shock", currentShock - m_ExtCLDexistingConfig.PenaltyShock);
        
        player.GetStatEnergy().Add( -m_ExtCLDexistingConfig.PenaltyHunger );
        player.GetStatWater().Add( -m_ExtCLDexistingConfig.PenaltyThirst );
        
        player.SetBloodyHands( m_ExtCLDexistingConfig.PenaltyBloodyHands);
        Print("[DZR_CLD] DelPen :: 402");
        GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(SetHealthCB, 3000, false, player, "", "Shock", currentShock - m_ExtCLDexistingConfig.PenaltyShock);
        //player.SetHealth("", "Shock", currentShock - m_ExtCLDexistingConfig.PenaltyShock);
        string extraText;
        Print("[DZR_CLD] DelPen :: 406");
        if(m_ExtCLDexistingConfig.NotifyOnDiscord)
        {
            if(currentHealth <= m_ExtCLDexistingConfig.PenaltyHealth)
            {
                extraText = "\r\n Current health"+": "+currentHealth+". :skull_crossbones: "+"Character will probably die after the penalty.";
            }
            
            string d_Message = "";
            string theUid = "";
            if(m_ExtCLDexistingConfig.ShowPlayerUID)
            {
                theUid = " ("+plainid+")";
            }
            
            
            
            if(m_ExtCLDexistingConfig.ShowPenaltyDetailsInDiscord)
            {
                if(m_ExtCLDexistingConfig.DiscordCustomMessage != "")
                {
                    d_Message += m_ExtCLDexistingConfig.DiscordCustomMessage+" ["+"Player"+" "+name+theUid+ "]\r\n";
                }
                {
                    //d_Message += ":warning: **"+"Player"+" "+name+theUid+ "  "+"disconnected in Combat Mode and received the following penalties:"+"**";
                    d_Message += string.Format(":warning: ** %1 "+name+theUid+ "  %2**", "Player", "disconnected in Combat Mode and received the following penalties:");
                }
                
                if(m_ExtCLDexistingConfig.PenaltyTimer != 0){
                    d_Message += "\r\n:hourglass_flowing_sand: Logout was delayed for **"+m_ExtCLDexistingConfig.PenaltyTimer+" sec.**";
                };
                
                if(m_ExtCLDexistingConfig.PenaltyHealth != 0){
                    d_Message += "\r\n:broken_heart: Health **-"+m_ExtCLDexistingConfig.PenaltyHealth+"."+extraText+"**";
                };
                if(m_ExtCLDexistingConfig.PenaltyBlood != 0){
                    d_Message += "\r\n:drop_of_blood: Blood **-"+m_ExtCLDexistingConfig.PenaltyBlood+"**";
                }
                if(m_ExtCLDexistingConfig.PenaltyShock != 0){
                    d_Message += "\r\n:anger: Shock **-"+m_ExtCLDexistingConfig.PenaltyShock+"**";
                }
                if(m_ExtCLDexistingConfig.PenaltyHunger != 0){
                    d_Message += "\r\n:apple: Energy **-"+m_ExtCLDexistingConfig.PenaltyHunger+"**";
                }
                if(m_ExtCLDexistingConfig.PenaltyThirst != 0){
                    d_Message += "\r\n:droplet: Water **-"+m_ExtCLDexistingConfig.PenaltyThirst+"**";
                }
                if(m_ExtCLDexistingConfig.PenaltyBloodyHands){
                    d_Message += "\r\n:palms_up_together: Bloody hands";
                }
                if(m_ExtCLDexistingConfig.PenaltyBreakLegs){
                    d_Message += "\r\n:wheelchair: Broken legs";
                }
                if(m_ExtCLDexistingConfig.PenaltyDisieaseAgents.Count() >= 1){
                    d_Message += "\r\n:microbe: Ifections added: **"+m_ExtCLDexistingConfig.PenaltyDisieaseAgents.Count()+"**";
                }
                if(m_ExtCLDexistingConfig.PenaltyGiveItems.Count() >= 1){
                    d_Message += "\r\n:gift: Items added: **"+m_ExtCLDexistingConfig.PenaltyGiveItems.Count()+"**";
                }
                if(m_ExtCLDexistingConfig.KillInstantPenalty){
                    d_Message += "\r\n:oncoming_police_car: Player kill penalty";
                }
                if(m_ExtCLDexistingConfig.PenaltyTeleport){
                    d_Message += "\r\n:flying_saucer: Teleported";
                }
                if(m_ExtCLDexistingConfig.PenaltyRemoveGear){
                    d_Message += "\r\n:customs: All gear removed";
                }
                else
                {
                    d_Message += ":warning: Player **"+name+theUid+ " disconnected in Combat Mode and received the following penalties:**";
                }
                theUid = "";
            };
            sendToDiscord(d_Message, m_ExtCLDexistingConfig.DiscordUrl);
            //Print(identity.GetPlainId()+" disconnected while in Combat Mode. received -"+m_CLDexistingConfig.PenaltyHealth+" health penalty."+extraText);
            GetDayZGame().Debug("___________________"+player+" Penalized_________________________");
        }
        
        
        //isWaitingForPenalty(player, "DelayedPenalties END");
    }
    
    void SetHealthCB(PlayerBase p_player, string s_zone, string s_type, float f_value)
    {
        Print("[DZR_CLD] DelPen :: 491");
        p_player.SetHealth(s_zone, s_type, f_value);
    }
    
    //Print("authFailed2: "+authFailed);
    
    
    void Debug(string strDebugMessage, int priority = 0, bool Enabled = false)
    {
        if(Enabled){
            string m_modName = "[DZR Combat Log Detection DZR_CLD.c]";
            string m_side = "NULL";
            
            string delimiter = " ::::::::: ";
            
            if(priority == 1)
            {
                delimiter = " ■ ■ ■ ■ ■ ";	
            }
            if(priority == 2)
            {
                delimiter = " ■■■■■■■■■ ";
            }
            
            //PlayerBase theClient = this;
            
            PlayerBase theClient = PlayerBase.Cast( GetGame().GetPlayer() );
            
            if(GetGame().IsClient())
            {
                m_side = "Clientside";
            };
            
            if(GetGame().IsServer())
            {
                m_side = "Serverside";
            };
            
            int hour;
            int minute;
            int second;
            
            GetHourMinuteSecond(hour, minute, second);
            //GetYearMonthDay(year, month, day);
            string suffix = hour.ToString()+":"+minute.ToString()+":"+second.ToString();
            
            if(theClient && theClient.GetIdentity())
            {
                GetGame().Chat(m_modName+delimiter+strDebugMessage, "colorImportant");
                Print("■"+suffix+"■ "+m_modName+delimiter+strDebugMessage+" ::: "+m_side);
                Param1<string> Msg = new Param1<string>( strDebugMessage );
                GetGame().RPCSingleParam( theClient, ERPCs.RPC_USER_ACTION_MESSAGE, Msg, true, theClient.GetIdentity() );
            }
        }
    }
    //Lock doors
    void LockDoors(vector position, int radius)
    {    
        
        
        vector pos = position;
        ref array<Object> nearest_objects = new array<Object>;
        ref array<CargoBase> proxy_cargos = new array<CargoBase>;
        GetGame().GetObjectsAtPosition ( pos, radius, nearest_objects, proxy_cargos );
        
        for ( int i = 0; i < nearest_objects.Count(); i++ )
        {
            Object nearest_object = nearest_objects.Get(i);
            
            if ( nearest_object.IsInherited( Building ) )
            {    
                
                Building building = Building.Cast( nearest_object );
                int doorsCount = building.GetGame().ConfigGetChildrenCount("CfgVehicles " + building.GetType() + " Doors");                
                for (int c = 0; c < doorsCount; c++)
                {
                    //vector distanceRoot = vector.DistanceSq(player.GetPosition(), playerRootPos);
                    //if( this.IsInReach(player, target, radius) && !building.IsDoorOpen(c) ) 
                    //{
                    if ( building.IsDoorOpen(c) ) building.CloseDoor(c);
                    building.LockDoor(c);
                    //}
                    
                    
                }
            }    
        }
    }
    void sendToDiscord(string message, string webhookURL)
    {
        if(m_CLDexistingConfig.NotifyOnDiscord)
        {
            // Compose the message to be sent to Discord.
            CLDDiscordMessage3 m_CLDDiscordMessage = new CLDDiscordMessage3();
            m_CLDDiscordMessage.username = "Combat Log Detection";
            m_CLDDiscordMessage.content = message;
            m_CLDDiscordMessage.avatar = "https://cdn.discordapp.com/attachments/948492094564089906/948848483157307432/unknown.png";
            
            // Convert that message into JSON format.
            string discordMessageJSON;
            JsonSerializer jsonSerializer = new JsonSerializer();
            jsonSerializer.WriteToString(m_CLDDiscordMessage, false, discordMessageJSON);
            //https://cdn.discordapp.com/attachments/948492094564089906/948848483157307432/unknown.png
            // Post the message to Discord.
            private RestApi restAPI;
            private RestContext restContext;
            webhookURL.Replace("https://discord.com/api/webhooks/", "");
            
            if(GetGame().IsServer())
            {
                restAPI = CreateRestApi();
                restContext = restAPI.GetRestContext("https://discord.com/api/webhooks/");
                restContext.SetHeader("application/json");
                restContext.POST(NULL, webhookURL, discordMessageJSON);
                
                //GetDayZGame().Debug("m_CLDexistingConfig.NotifyOnDiscord: " + m_CLDDiscordMessage.content);
            }
        }
    }
}	    