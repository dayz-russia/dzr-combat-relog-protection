class CLDDiscordMessage2
{
	string username;
	string content;
	string avatar;
}


modded class PlayerBase
{
    protected bool m_isInCombat;
    protected bool m_isCombatTimerReset;
    protected bool m_hasTimer;
    protected bool m_isPrisoner;
    protected bool m_isPrisonTimerRunning;
    
    protected bool ExitButtonAllowed = false;
    protected bool m_isWaitingForPenalty = false;
	
	ref dzr_cld_config_data_class m_CLDexistingConfig;
	
	DZR_CLD m_dzrCLD;
	
	override void Init()
	{
		
		
		
		GetRPCManager().AddRPC( "DZR_CLD_RPC", "ReadConfig", this, SingleplayerExecutionType.Both );
		
		if (GetGame().IsServer())
		m_dzrCLD = DZR_CLD.Cast(GetPlugin(DZR_CLD));
		
		super.Init();
		
        
    }
	
	void readConfig( CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target )
	{
		
    }
	
	
	bool IsPrisoner()
    {
        return m_isPrisoner;
    }
	
	void SetPrisonTimerRunning(bool value)
    {
        m_isPrisonTimerRunning = value;        
    }
	
	bool IsPrisonTimerRunning()
    {
        return m_isPrisonTimerRunning;        
    }
	
	void SetPrisoner(bool value)
    {
        m_isPrisoner = value;        
    }
    
	override void OnConnect()
	{
		super.OnConnect();
		
		
		//this.Debug("readConfig requested");
		m_CLDexistingConfig = GetDayZGame().ModConfig("PlayerBase.c");
		this.Debug("PlayerBase OnConnect", 0, m_CLDexistingConfig.EnableDebug);
		this.Debug("PlayerBase ::: ACTIVE (Version: "+m_CLDexistingConfig.ModVersion+"."+m_CLDexistingConfig.ConfigVersion+" DEBUG: "+m_CLDexistingConfig.EnableDebug+")", 0, m_CLDexistingConfig.EnableDebug);
		this.Debug("PenaltyTimer: "+m_CLDexistingConfig.PenaltyTimer, 0, m_CLDexistingConfig.EnableDebug);
		this.Debug("CombatCheckRadius: "+m_CLDexistingConfig.CombatCheckRadius, 0, m_CLDexistingConfig.EnableDebug);
		this.Debug("NotifyOnDiscord: "+m_CLDexistingConfig.NotifyOnDiscord, 0, m_CLDexistingConfig.EnableDebug);
		//this.Debug("ExitButtonNoPenalty: "+m_CLDexistingConfig.ExitButtonNoPenalty);
		
    }
	
	dzr_cld_config_data_class playerConfig()
	{
		return m_CLDexistingConfig;
    }
	
	bool isBotAI ()
	{
		PlayerBase player = PlayerBase.Cast(this);
		
		if (player.IsPlayer())
		{
			return false;
        }
		return true;
    }
	
	override void EEKilled( Object killer )
	{
		//this.instantPrisoner( killer, PlayerBase.Cast(this) );
		super.EEKilled(killer);
		if (m_dzrCLD)
		{
			//Print("PlayerBase.c EEKilled() - m_dzrCLD exists.");
			GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(m_dzrCLD.PlayerKilled, 1500, false, PlayerBase.Cast(this), killer);
			//m_dzrCLD.PlayerKilled(PlayerBase.Cast(this), killer);
        }
		
		
    }
	
	void instantPrisoner(Object killer, PlayerBase player)
	{
		player.Debug(killer.GetDisplayName() +" killed "+PlayerBase.Cast(player).GetIdentity().GetName(), 0, m_CLDexistingConfig.EnableDebug);
		if (player && killer && player.GetIdentity() != NULL)
		{
			
			if ( killer.IsWeapon() || killer.IsMeleeWeapon() )
			{				
				
				PlayerBase otherPlayer = PlayerBase.Cast( EntityAI.Cast( killer ).GetHierarchyParent() );
				if(otherPlayer)
				{
					
					player.Debug( otherPlayer.GetIdentity().GetName()+ " killed "+ player.GetIdentity().GetName() +". Applying penalties 1.", 0, m_CLDexistingConfig.EnableDebug);
					//otherPlayer.Debug( otherPlayer.GetIdentity().GetName()+ " killed "+ player.GetIdentity().GetName() +". Applying penalties 2.");
					player.Debug("You are killed.", 0, m_CLDexistingConfig.EnableDebug);
					//otherPlayer.Debug("You are the killer.");
					ref Param1<PlayerBase> m_Data = new Param1<PlayerBase>(otherPlayer);
					//PlayerIdentity identity, PlayerBase player, bool authFailed, string name, string plainid, int logout
					GetRPCManager().SendRPC( "DZR_CLD_RPC", "instantPenalty", m_Data, true, player.GetIdentity());
					//applyInstantPenalties(  );
					
                }
            }
        }
    }
	
    
	void removeDelayedPenalties(PlayerBase player)
	{
		this.Debug("Trying to remove delayed penalties", 1, m_CLDexistingConfig.EnableDebug);
		ref Param1<PlayerBase> m_Data = new Param1<PlayerBase>(this);
		//m_dzrCLD
		GetRPCManager().SendRPC( "DZR_CLD_RPC", "removePenalty", m_Data, true, player.GetIdentity());
    }
	
	override void EEHitBy(TotalDamageResult damageResult, int damageType, EntityAI source, int component, string dmgZone, string ammo, vector modelPos, float speedCoef)
	{
		PlayerBase theSource;
        bool register_damage = false;
		float m_Distance = vector.Distance( PlayerBase.Cast(this).GetPosition(), source.GetPosition() );
		Print("HIT DISTANCE: "+m_Distance);
		if(m_Distance >= m_CLDexistingConfig.MaxHitDistance_m && m_CLDexistingConfig.MaxHitDistance_m != 0.0)
		{
			theSource = PlayerBase.Cast( EntityAI.Cast( source ).GetHierarchyParent() );
			if(theSource.IsPlayer())
			{
                m_dzrCLD.DelayedPenalties( theSource.GetIdentity(),  theSource,  false,  theSource.GetIdentity().GetName(),  theSource.GetIdentity().GetPlainId(),  4, m_CLDexistingConfig);
				string warningTitle = "Max hit distance penalty";
				string warningText = "No hits allowed beyond "+ m_CLDexistingConfig.MaxHitDistance_m + " meters.";
				
				NotificationSystem.SendNotificationToPlayerIdentityExtended( theSource.GetIdentity() , m_CLDexistingConfig.CombatModeCooldownTime_s, warningTitle, warningText, m_CLDexistingConfig.PlayerCombatWarningIcon);
				
				Print("Max hit distance penalty");
            }
			
        }
        else
        {
            super.EEHitBy( damageResult,  damageType,  source,  component,  dmgZone,  ammo,  modelPos,  speedCoef);
        }
        
        m_dzrCLD.PlayerHit(PlayerBase.Cast(this), source); 
        
        
		
    }
	
	bool isInCombat()
	{
		return m_isInCombat;
    }
	
	bool isCombatTimerReset()
	{
		return m_isCombatTimerReset;
    }
	
	void setCombatState(bool val)
	{
		if(!val)
		{
			m_isCombatTimerReset = false;
        }
		m_isInCombat = val;
    }
	
	
	void resetCombatTimer()
	{
		
		m_isCombatTimerReset = true;
		
    }
	
	bool hasTimer(string let = "get", bool val = false)
	{
		if(let == "set")
		{
			m_hasTimer = val;
        }
		return m_hasTimer;
		
    }
	
	
	
	
	bool canExitEarly( string cmd = "get", bool val = false, bool ExitTrigger = false )
	{
		ExitButtonAllowed = false;
		this.Debug(":::::::::::::: |||||||||||| Server called canExitEarly", 0, m_CLDexistingConfig.EnableDebug);
		
		//this.Debug( "canExitEarly SRV: " + GetGame().IsServer().ToString() + " CNT: " + GetGame().IsClient().ToString());
		if(cmd == "set")
		{
			ExitButtonAllowed = val;
        }
		/*
			if(val && m_CLDexistingConfig.ExitButtonNoPenalty)
			{
			this.setCombatMode(false, "PlayerBase.c");
			
			}
        */
		if(cmd == "LogoutMenu")
		{
			this.Debug(":::::::::::::: Server requested safe logout: cmd:"+cmd+" val:" +val, 1, m_CLDexistingConfig.EnableDebug);
			this.Debug("canExitEarly: with LogoutMenu cmd:"+cmd+" val:" +val+ " for player: "+this, 0, m_CLDexistingConfig.EnableDebug);
			ExitButtonAllowed = val;
			this.setCombatMode(ExitTrigger, "PlayerBase.c");
			GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(this.DelayedAbort, 100, false, false);
			
        }
		return ExitButtonAllowed;
    }
	
	void DelayedAbort()
	{
		this.Debug("DelayedAbort Activate", 2, m_CLDexistingConfig.EnableDebug);
		//ref Param1<PlayerBase> m_Data = new Param1<PlayerBase>(thePlayer);
		GetRPCManager().SendRPC( "DZR_CLD_RPC", "AbortMission", NULL, true, this.GetIdentity());
    }
	
	void setCombatMode(bool val = false, string debugMessage = "not specified.")
	{
		PlayerBase player = PlayerBase.Cast(this);
		if(player && player.IsPlayer() && m_CLDexistingConfig && player.GetIdentity() != NULL)
		{
			//this.Debug("setting setCombatMode to "+val);
			//this.Debug("Checking if player has timer: "+player.hasTimer());
			if( val )
			{
				
				
				//GetDayZGame().Debug("Checking if player has timer: "+player.hasTimer());
				if(player.hasTimer())
				{
					//this.Debug("Player had timer = Removed Timer");
					GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(setCombatMode);
					//this.Debug("Added new: "+m_CLDexistingConfig.PenaltyTimer);
					GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(setCombatMode, (m_CLDexistingConfig.CombatModeCooldownTime_s*1000), false, false, "");
                }
				else
				{
					//this.Debug("Player had no timer = Added new: "+m_CLDexistingConfig.PenaltyTimer);
					GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(setCombatMode, (m_CLDexistingConfig.CombatModeCooldownTime_s*1000), false, false, "");
                }
				
				
				player.hasTimer("set" ,true);
				player.setCombatState(val);	
				
				
				string theMessage = player.GetIdentity().GetName()+ " ("+player.GetIdentity().GetPlainId()+"): Combat Mode -ON- for "+ (m_CLDexistingConfig.CombatModeCooldownTime_s) +" s. ::: Source: "+debugMessage;
				this.Debug(theMessage, 0, m_CLDexistingConfig.EnableDebug);
				
				
            }
			else
			{
				if(player.GetIdentity())
				{
					this.Debug("Timer up = disabling Combat Mode ", 0, m_CLDexistingConfig.EnableDebug);
					player.hasTimer("set", false);
					player.setCombatState(false);
					this.Debug("Removing any timers", 0, m_CLDexistingConfig.EnableDebug);
					GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(setCombatMode);
					
					
					string theMessage2 = player.GetIdentity().GetName()+ " ("+player.GetIdentity().GetPlainId()+"): Combat Mode -OFF-.";
					this.Debug(theMessage2, 0, m_CLDexistingConfig.EnableDebug);
					this.removeDelayedPenalties(player);
                }
            }
        }
		//return true;
		//player.setCombatTimer(false);
    }
	
	override void OnDisconnect()
	{
		//this.Debug("PlayerBase OnDisconnect", 0, m_CLDexistingConfig.EnableDebug);
		//GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(setCombatMode);
		
		super.OnDisconnect();
    }
	
	void sendPlayerMessage(PlayerBase player, string theMessage)
	{
		Param1<string> Msg = new Param1<string>( theMessage );
		GetGame().RPCSingleParam( player, ERPCs.RPC_USER_ACTION_MESSAGE, Msg, true, player.GetIdentity() );
    }
	
	void Debug(string strDebugMessage, int priority = 0, bool Enabled = false)
	{
		if(Enabled){
			string m_modName = "[DZR Combat Log Detection PlayerBase.c]";
			string m_side = "NULL";
			
			string delimiter = " ::::::::: ";
			
			if(priority == 1)
			{
				delimiter = " ■ ■ ■ ■ ■ ";	
            }
			if(priority == 2)
			{
				delimiter = " ■■■■■■■■■ ";
            }
			
			PlayerBase theClient = this;
			
			
			
			if(GetGame().IsClient())
			{
				m_side = "Clientside";
            };
			
			if(GetGame().IsServer())
			{
				m_side = "Serverside";
            };
			
			
			int hour;
			int minute;
			int second;
			
			GetHourMinuteSecond(hour, minute, second);
			//GetYearMonthDay(year, month, day);
			string suffix = hour.ToString()+":"+minute.ToString()+":"+second.ToString();
			
			if(theClient && theClient.GetIdentity())
			{
				GetGame().Chat(m_modName+delimiter+strDebugMessage, "colorImportant");
				Print("■"+suffix+"■ "+m_modName+delimiter+strDebugMessage+" ::: "+m_side);
				Param1<string> Msg = new Param1<string>( strDebugMessage );
				GetGame().RPCSingleParam( theClient, ERPCs.RPC_USER_ACTION_MESSAGE, Msg, true, theClient.GetIdentity() );
            }
        }
    }
	
	
	void sendToDiscord(string message, string webhookURL)
	{
		if(m_CLDexistingConfig.NotifyOnDiscord)
		{
			// Compose the message to be sent to Discord.
			CLDDiscordMessage2 m_CLDDiscordMessage = new CLDDiscordMessage2();
			m_CLDDiscordMessage.username = "Combat Log Detection";
			m_CLDDiscordMessage.content = message;
			m_CLDDiscordMessage.avatar = "https://cdn.discordapp.com/attachments/948492094564089906/948848483157307432/unknown.png";
			
			// Convert that message into JSON format.
			string discordMessageJSON;
			JsonSerializer jsonSerializer = new JsonSerializer();
			jsonSerializer.WriteToString(m_CLDDiscordMessage, false, discordMessageJSON);
			//https://cdn.discordapp.com/attachments/948492094564089906/948848483157307432/unknown.png
			// Post the message to Discord.
			private RestApi restAPI;
			private RestContext restContext;
			webhookURL.Replace("https://discord.com/api/webhooks/", "");
			
			if(GetGame().IsServer())
			{
				restAPI = CreateRestApi();
				restContext = restAPI.GetRestContext("https://discord.com/api/webhooks/");
				restContext.SetHeader("application/json");
				restContext.POST(NULL, webhookURL, discordMessageJSON);
				
				//GetDayZGame().Debug("m_CLDexistingConfig.NotifyOnDiscord: " + m_CLDDiscordMessage.content);
            }
        }
    }
	
	void stripPlayer()
	{
		PlayerBase thePlayer = this;
		ItemBase HandsItem = thePlayer.GetItemInHands();
		if(HandsItem)
		{
			HandsItem.Delete();
        }
		ItemBase theItem = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "Legs" ));
		if(theItem)
		{ 
			thePlayer.GetInventory().LocalDestroyEntity(theItem); 
        };
		theItem = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "Body" ));
		if(theItem)
		{ 
			thePlayer.GetInventory().LocalDestroyEntity(theItem); 
        };
		theItem = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "Armband" ));
		if(theItem)
		{ 
			thePlayer.GetInventory().LocalDestroyEntity(theItem); 
        };
		theItem = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "Headgear" ));
		if(theItem)
		{ 
			thePlayer.GetInventory().LocalDestroyEntity(theItem); 
        };
		theItem = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "Hands" ));
		if(theItem)
		{ 
			thePlayer.GetInventory().LocalDestroyEntity(theItem); 
        };
		theItem = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "Eyewear" ));
		if(theItem)
		{ 
			thePlayer.GetInventory().LocalDestroyEntity(theItem); 
        };
		theItem = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "Feet" ));
		if(theItem)
		{ 
			thePlayer.GetInventory().LocalDestroyEntity(theItem); 
        };
		theItem = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "Vest" ));
		if(theItem)
		{ 
			thePlayer.GetInventory().LocalDestroyEntity(theItem); 
        };
		theItem = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "Gloves" ));
		if(theItem)
		{ 
			thePlayer.GetInventory().LocalDestroyEntity(theItem); 
        };
		theItem = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "Back" ));
		if(theItem)
		{ 
			thePlayer.GetInventory().LocalDestroyEntity(theItem); 
        };
		theItem = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "Shoulder" ));
		if(theItem)
		{ 
			thePlayer.GetInventory().LocalDestroyEntity(theItem); 
        };
		theItem = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "Bow" ));
		if(theItem)
		{ 
			thePlayer.GetInventory().LocalDestroyEntity(theItem); 
        };
		theItem = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "Hips" ));
		if(theItem)
		{ 
			thePlayer.GetInventory().LocalDestroyEntity(theItem); 
        };
		theItem = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "Melee" ));
		if(theItem)
		{ 
			thePlayer.GetInventory().LocalDestroyEntity(theItem); 
        };
		theItem = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "Mask" ));
		if(theItem)
		{ 
			thePlayer.GetInventory().LocalDestroyEntity(theItem); 
        };
    }
	
	bool isWaitingForPenalty(string source = "")
	{
		PlayerBase player = this;
		PlayerBase listPlayer;
		this.Debug(":::::::::::::::: m_WaitingForPenalty LIST "+source+" ::::::::::::::::", 0, m_CLDexistingConfig.EnableDebug);
		
		
		if(player && player.IsPlayer())
		{
			if(m_isWaitingForPenalty && player.GetIdentity() && m_isInCombat)
			{
				this.Debug("Player already waiting for penalty :"+player.GetIdentity().GetName() + "("+player.GetIdentity().GetPlainId()+")", 1, m_CLDexistingConfig.EnableDebug);
				return true;
            }
			
			this.Debug("Player "+player+ " not found", 1, m_CLDexistingConfig.EnableDebug);
        }
		this.Debug(":::::::::::::::::::::::::::::::::::::::::::::::::::::::", 0, m_CLDexistingConfig.EnableDebug);
		return false;
    }
}	

