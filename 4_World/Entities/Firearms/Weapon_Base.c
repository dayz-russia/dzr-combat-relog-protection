
//TODO read config on init only.
modded class Weapon_Base
{
	
	override void EEFired(int muzzleType, int mode, string ammoType)
	{	
		super.EEFired(muzzleType, mode, ammoType);
		dzr_cld_config_data_class m_CLDexistingConfig;
		if ( isServerSide() )
		{
			
			if(!m_CLDexistingConfig)
			{
				m_CLDexistingConfig = GetDayZGame().ModConfig("Weapon_Base");
			};
			
			//m_CLDexistingConfig = GetDayZGame().ModConfig();
			Param1<string> Msg;		
			PlayerBase player = PlayerBase.Cast(GetHierarchyRootPlayer());
			//string PlayerID = player.GetIdentity().GetPlainId();
			if(player && m_CLDexistingConfig.WeaponFireTrigger)
			{
				player.setCombatMode(true);
				player.resetCombatTimer();
				if(player.GetIdentity())
				{
					string theMessage = player.GetIdentity().GetName()+ " ("+player.GetIdentity().GetPlainId()+") Is shooting and now in combat mode for "+ (m_CLDexistingConfig.CombatModeCooldownTime_s) +" s. ::: Weapon_Base";
					player.Debug(theMessage, 2, m_CLDexistingConfig.EnableDebug);
				};
			}
			
			
			
		}
	}
	
	bool isServerSide()
	{
		if ( GetGame().IsMultiplayer() && GetGame().IsServer() )
		{
			if (GetGame().GetMission())
			{
				return true;
			}
		}
		return false;
	}
	
};

