////////////////////////////////////////////////////////////////////
//DeRap: dzr_combat_log_detection\config.bin
//Produced from mikero's Dos Tools Dll version 7.97
//https://mikero.bytex.digital/Downloads
//'now' is Mon Jun 13 15:54:17 2022 : 'file' last modified on Fri Jun 03 09:19:09 2022
////////////////////////////////////////////////////////////////////

#define _ARMA_

class CfgPatches
{
	class dzr_combat_log_detection
	{
		requiredAddons[] = {"DZ_Data"};
		units[] = {};
		weapons[] = {};
	};
};
class CfgMods
{
	class dzr_combat_log_detection
	{
		type = "mod";
		author = "DayZRussia.com";
		description = "If a player tries to disconnect during combat or to avoid threats, admins can set up reaction to this: logout timer pause or increase, discord notification, kill on disconnect, etc.";
		dir = "dzr_combat_log_detection";
		name = "DZR Combat Log Detection";
		dependencies[] = {"Core","Game","World","Mission"};
		class defs
		{
			class engineScriptModule
			{
				files[] = {"dzr_combat_log_detection/Common","dzr_combat_log_detection/1_Core"};
			};
			class gameScriptModule
			{
				files[] = {"dzr_combat_log_detection/3_Game"};
			};
			class worldScriptModule
			{
				files[] = {"dzr_combat_log_detection/4_World"};
			};
			class missionScriptModule
			{
				files[] = {"dzr_combat_log_detection/5_Mission"};
			};
		};
	};
};
