modded class MissionGameplay
{
	bool m_CLD_LocalHold;
    
    static ref DZR_CLD m_CLD;
	ref dzr_cld_config_data_class m_CLDexistingConfig;
	static ref dzr_cld_CFG_class dzr_cld_CFG;
    
	override void OnInit()
	{
		super.OnInit();
		GetRPCManager().AddRPC( "DZR_CLD_RPC", "AbortMission", this, SingleplayerExecutionType.Both );
        m_CLD = new DZR_CLD(m_CLDexistingConfig);
        dzr_cld_CFG = new dzr_cld_CFG_class();
    }
    
    
    override void CreateLogoutMenu(UIMenuPanel parent)
	{
        PlayerBase player = PlayerBase.Cast( GetGame().GetPlayer() );
        Print("Is In Combat? " +player.isInCombat())
        Print("DisableExitButtonInCombatMode? " +m_CLDexistingConfig.DisableExitButtonInCombatMode)
        if(player.isInCombat() && m_CLDexistingConfig.DisableExitButtonInCombatMode)
        {
            NotificationSystem.SendNotificationToPlayerIdentityExtended( player.GetIdentity() , 10,  "Combat Mode Active", "Exit button disabled in Combat Mode.", m_CLDexistingConfig.PlayerCombatWarningIcon);
            return;
        }
        super.CreateLogoutMenu(parent);
    }
    
	bool ActivePrisoners()
	{
		string fullPath = "$profile:/DZR/CombatLogDetection/Prisoners/";
		string	file_name;
		int 	file_attr;
		int		flags;
        
        
		//string path_find_pattern = m_DZRdir+"\\*"; //*/
		//string fullPath = rootDir+"\\"+m_DZRdir+"\\";
		//Print("Trying to find all "+extension+" files in "+fullPath);
		FindFileHandle file_handler = FindFile(fullPath+"/*.txt", file_name, file_attr, flags);
		
		//Print("file_name: "+file_name);
		
		//ext, count, path, map(raw_name, lines_array)
		
		bool found = true;
		int counter = 0;
		bool filesExist = false;
		
		FileHandle fhandle;
		while ( found )
		{				
			
			//Print("Found "+extension+" file: "+file_name);
			
			if (FileExist(fullPath+"\\"+file_name))
			{
				filesExist = true;
				fhandle	=	OpenFile(fullPath+"\\"+file_name, FileMode.READ);
				string line_content;
				ref array<string> content = new array<string>;
				while ( FGets( fhandle,  line_content ) > 0 )
				{
					content.Insert(line_content);
                }
				CloseFile(fhandle);
				
				counter = counter + 1;
				//files_return_array.fileArray.Insert(file_name, content);
            }
			found = FindNextFile(file_handler, file_name, file_attr);
			//files_return_array.count = counter;	
        }
		//Print("counter: "+counter);
		return filesExist;
    }
    
}