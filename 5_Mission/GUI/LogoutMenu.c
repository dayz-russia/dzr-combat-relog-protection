modded class LogoutMenu 
{	
	PlayerBase thePlayer;
	bool isWaitingForLogout = false;
	override bool OnClick(Widget w, int x, int y, int button)
	{
		isWaitingForLogout = false;
		if (w == m_bLogoutNow)
		{
			thePlayer = PlayerBase.Cast( GetGame().GetPlayer() );
			
			//Print( "m_bLogoutNow SRV: " + GetGame().IsServer().ToString() + " CNT: " + GetGame().IsClient().ToString());
			//Print("m_bLogoutNow: Player Used Legal Button. Checking.");
			
			//Print("m_bLogoutNow: Player Used Legal Button. No Penalty.");
			if ( GetGame().IsClient() )
			{
				ref Param1<PlayerBase> m_Data = new Param1<PlayerBase>(thePlayer);
				GetRPCManager().SendRPC( "DZR_CLD_RPC", "remoteCanExitEarly", m_Data, true, thePlayer.GetIdentity());
				thePlayer = NULL;
				isWaitingForLogout = true;
				//return true;
			}

			
		}
		if (!isWaitingForLogout)
		{
			return super.OnClick( w,  x,  y,  button);
		}
		return false;
	}
	
	void delayedAbort()
	{

			GetGame().GetMission().AbortMission();
			
			
	}
	
	void ~LogoutMenu()
	{
		thePlayer = PlayerBase.Cast( GetGame().GetPlayer() );
		ref Param1<PlayerBase> m_Data = new Param1<PlayerBase>(thePlayer);
		GetRPCManager().SendRPC( "DZR_CLD_RPC", "removePenalty", m_Data, true, thePlayer.GetIdentity());
		thePlayer = NULL;
		g_Game.SetKeyboardHandle(NULL);
	}
	
}

