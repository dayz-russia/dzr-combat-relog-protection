class CLDDiscordMessage
{
	string username;
	string content;
	string avatar;
}


modded class MissionServer
{
    
    ref array<string> m_NullArray = new array<string>;
    
	ref dzr_cld_config_data_class m_CLDdefaultConfig;
	ref dzr_cld_config_data_class m_CLDexistingConfig;
	const static string dzr_Cld_ProfileFolder = "$profile:\\";
	const static string dzr_Cld_TagFolder = "DZR\\";
	const static string dzr_Cld_ModFolder = "CombatLogDetection\\";
    const static string dzr_Cld_ConfigFile = "dzr_cld_config.json";	
	ref array<PlayerBase, bool> Players;
	//ref array<PlayerBase, bool> WaitingForLogout;
	ref array<PlayerBase, bool> WaitingForLogout = new array<PlayerBase, bool>;
	ref map<PlayerBase, bool> m_WaitingForPenalty = new map<PlayerBase, bool>;
	bool instantKillPenalty = false;
	//DZR_CLD m_dzrCLD;
	bool isWaitingForLogout = false;
    
	static ref DZR_CLD m_CLD;
	static ref dzr_cld_CFG_class dzr_cld_CFG;
	
	ref map<string, int> m_Prisoners;
	string m_StoreFolder = "$profile:/DZR/CombatLogDetection/Prisoners";
	int m_IncremenPrisontInterval = 60000;
	
	void MissionServer()
	{
		//GetDayZGame().Debug("[dzr_combat_log_detection] ::: Starting Serverside");
		//GetDayZGame().Debug("test", 0 );
		GetRPCManager().AddRPC( "DZR_CLD", "SERVER_SendToDiscord", this, SingleplayerExecutionType.Both );
		//GetRPCManager().AddRPC( "DZR_CLD", "ClientNeedsConfig", this, SingleplayerExecutionType.Both );
		//dzr_cld_config_data_class m_CLDexistingConfig = new dzr_cld_config_data_class();
		
		if( DZR_CLD_MissionServer_ReadConfigFile() )
		{
			//GetDayZGame().Debug("[DZR Combat Log Detection] ::: DZR_CLD_MissionServer_ReadConfigFile TRUE");
			//m_CLDexistingConfig = GetDayZGame().ModConfig();
			//GetDayZGame().Debug("[DZR Combat Log Detection] ::: m_CLDexistingConfig.EnableDebug: " + m_CLDexistingConfig.EnableDebug);
			//GetDayZGame().Debug("MissionServer start", 0, m_CLDexistingConfig.EnableDebug);
			Print("[DZR Combat Log Detection] ServerSide ::: ACTIVE (Version: "+m_CLDexistingConfig.ModVersion+"."+m_CLDexistingConfig.ConfigVersion+" DEBUG: "+m_CLDexistingConfig.EnableDebug+")");
			Print("PenaltyTimer: "+m_CLDexistingConfig.PenaltyTimer);
			Print("CombatCheckRadius: "+m_CLDexistingConfig.CombatCheckRadius);
			Print("NotifyOnDiscord: "+m_CLDexistingConfig.NotifyOnDiscord);
			m_CLD = new DZR_CLD(m_CLDexistingConfig);
			dzr_cld_CFG = new dzr_cld_CFG_class();
			//Print("ExitButtonNoPenalty: "+m_CLDexistingConfig.ExitButtonNoPenalty);
            
            m_Prisoners = new map<string, int>;
        }
		
		GetRPCManager().AddRPC( "DZR_CLD_RPC", "remoteCanExitEarly", this, SingleplayerExecutionType.Both );
		GetRPCManager().AddRPC( "DZR_CLD_RPC", "removePenalty", this, SingleplayerExecutionType.Both );
		GetRPCManager().AddRPC( "DZR_CLD_RPC", "instantPenalty", this, SingleplayerExecutionType.Both );
		GetRPCManager().AddRPC( "DZR_CLD_RPC", "usedAltF4", this, SingleplayerExecutionType.Both );
		
		GetRPCManager().AddRPC( "DZR_CLD_RPC", "StartPrisonTimerRPC", this, SingleplayerExecutionType.Both );
		
		GetDayZGame().Debug("Added RPC DZR_CLD_RPC", 0, m_CLDexistingConfig.EnableDebug);
		/*
			if (GetGame().IsServer())
			{
			m_dzrCLD = DZR_CLD.Cast(GetPlugin(DZR_CLD));
			}
        */
    }
	/*
		void ClientNeedsConfig()
		{
		ref Param1<dzr_cld_config_data_class> m_Data = new Param1<dzr_cld_config_data_class>(GetDayZGame().ModConfig());
		GetRPCManager().SendRPC( "DZR_CLD_RPC_TOclient", "DZR_CLD_MissionGameplay_SaveConfigOnClient", m_Data, true, sender);
		}
    */
	
    void StartPrisonTimerRPC(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
    {
        Print("Got RPC. Trying to continue.");
        ref Param1<PlayerBase> data;
        if ( !ctx.Read( data ) )
        {
            Print("No data from RPC");
        }
        Print("Data checked.");
        //this.Debug("data.param1 :"+data.param1);
        //this.Debug("Player received. Server satarts safe logout.");
        if (type == CallType.Server)
        {
            if (data.param1 != NULL)
            {
                Print("Got RPC data!. Starting Timer.");
                //StartPrisonTimer(data.param1);
            }
        }
    }
    
	void StartPrisonTimer(PlayerBase player, string SteamID)
	{
        Print("Trying to start Timer for "+ SteamID);
        if(player && m_CLDexistingConfig.TimeToKeepInPrison_m >= 1 && m_CLDexistingConfig.PenaltyTeleport)
        {
            int m_IntPrisonTime_Stored = 0;
            
            // string SteamID = player.GetIdentity().GetPlainId();
            
            if(FileExist(m_StoreFolder+"/"+SteamID+".txt") )
            {
                
                m_IntPrisonTime_Stored = dzr_cld_CFG.GetFile(m_StoreFolder, SteamID+".txt", m_NullArray).ToInt();
                Print("File exists. Setting to file: "+m_IntPrisonTime_Stored);
            }
            else 
            {
                int prisonTime = m_CLDexistingConfig.TimeToKeepInPrison_m;
                m_IntPrisonTime_Stored = dzr_cld_CFG.GetFile(m_StoreFolder, SteamID+".txt", m_NullArray, prisonTime.ToString() ).ToInt();
                Print("No file. Setting to config: "+m_IntPrisonTime_Stored);
            }
            
            if(m_Prisoners.Contains( SteamID ))
            {
                m_Prisoners.Set( SteamID, m_IntPrisonTime_Stored );
            }
            else
            {
                Print("New Prisoner");
                m_Prisoners.Insert( SteamID, m_IntPrisonTime_Stored );
            }
            
            if(m_Prisoners.Contains( SteamID ))
            {
                Print("Starting Timer.");
                player.SetPrisonTimerRunning(true);
                Print("■■■ IsPrisonTimerRunning."+ player.IsPrisonTimerRunning() );
                GetGame().GetCallQueue( CALL_CATEGORY_SYSTEM ).CallLater( this.AddPrisonerMinute, m_IncremenPrisontInterval, false, player, SteamID);
                //m_IntPrisonTime_Stored = dzr_cld_CFG.GetFile(m_StoreFolder, SteamID+".txt", m_NullArray).ToInt();
                
                string theMessage = "You will be released from prison after "+m_IntPrisonTime_Stored+" minutes";
                
                NotificationSystem.SendNotificationToPlayerIdentityExtended( player.GetIdentity() , 10,  "In Prison", theMessage, m_CLDexistingConfig.PlayerCombatWarningIcon);
            }
        }
    }
	
	bool isPrisoner(string SteamID)
	{
        //string SteamID = player.GetIdentity().GetPlainId();
        if(FileExist(m_StoreFolder+"/"+SteamID+".txt"))
        {
            return true;
        }
        
        return false;
    }
	
	void AddPrisonerMinute(PlayerBase player, string SteamID)
	{
        //Print(m_Prisoners);
        //string SteamID = player.GetIdentity().GetPlainId();
		if(player && m_Prisoners.Contains( SteamID ) && isPrisoner(SteamID) )
		{
			int increment = 0;
			if( player.IsAlive() )
			{
                //Print("Incremented timer");
                increment = 1;
            }
            else
            {
                DeleteFile(m_StoreFolder+"/"+SteamID+".txt");                
            }
            Print("■■■■ Removing 1 minute from " + m_Prisoners.Get( SteamID ) + "for "+ SteamID);
			//int m_IntPrisonTime_Stored = dzr_cld_CFG.GetFile(m_StoreFolder, SteamID+".txt", m_NullArray).ToInt();
			//Print("m_IntPrisonTime_Stored: "+m_IntPrisonTime_Stored);
            int CurrentTime = m_Prisoners.Get( SteamID ) - increment;
            Print("CurrentTime: "+CurrentTime);
			//CheckTime(CurrentTime, player);
			
			
			
			m_Prisoners.Set( SteamID, CurrentTime );
			//Print(m_Prisoners.Get( player ));
			if(CurrentTime >= 1)
			{
                GetGame().GetCallQueue( CALL_CATEGORY_SYSTEM ).CallLater( this.AddPrisonerMinute, m_IncremenPrisontInterval, false, player, SteamID);
                
                string theMessage = "You will be released from prison after "+CurrentTime+" minutes";
                
                NotificationSystem.SendNotificationToPlayerIdentityExtended( player.GetIdentity() , 10,  "In Prison", theMessage, m_CLDexistingConfig.PlayerCombatWarningIcon);
                
            }
			else
			{
                ReleasePrisoner(player);
            }
        }
    }
	
	override void OnClientReadyEvent(PlayerIdentity identity, PlayerBase player)
	{
        super.OnClientReadyEvent(identity, player);		
        
		string SteamID = identity.GetPlainId();
        Print("Client ready. Checking prisoners for " + SteamID);
        
        if(isPrisoner(SteamID))
        {
            Print(SteamID + " is prisoner by file");  
            int m_IntPrisonTime_Stored = dzr_cld_CFG.GetFile(m_StoreFolder, SteamID+".txt", m_NullArray).ToInt();
            if(m_IntPrisonTime_Stored <= 0)
            {
                Print("Releasing "+SteamID);
                ReleasePrisoner(player);
            }
            else
            {
                Print("Resuming timer "+SteamID);
                StartPrisonTimer(player, SteamID);
            }
        }
        
        GetGame().GetCallQueue( CALL_CATEGORY_SYSTEM ).CallLater( this.CheckIfPrisoner, 5000, false, player, SteamID);
		
        SteamID = "";
        
        
    }
    
    void CheckIfPrisoner(PlayerBase player, string SteamID)
    {
        if(player)
        {
            Print("Checking prisoner: "+SteamID);
            Print("isPrisoner: "+isPrisoner(SteamID));
            Print("Timer running?: "+player.IsPrisonTimerRunning());
            //Print(isPrisoner(SteamID));
            //Print(!player.IsPrisonTimerRunning());
            if(isPrisoner(SteamID) && !player.IsPrisonTimerRunning() )
            {
                
                StartPrisonTimer(player, SteamID);
                
            }
            else
            {
                GetGame().GetCallQueue( CALL_CATEGORY_SYSTEM ).CallLater( this.CheckIfPrisoner, 5000, false, player, SteamID);
            }
        }
    }
    
    void StopPrisonTimer(PlayerBase player, string SteamID)
    {
        player.SetPrisonTimerRunning(false);
        Print("■■■■ Stopping prison timer and saving: " + SteamID);
        Print(m_Prisoners);
        if(m_Prisoners.Contains( SteamID ))
        {
            //string SteamID = player.GetIdentity().GetPlainId();
            Print("Setting prison timer file to "+m_Prisoners.Get( SteamID ).ToString() );
            string StoredTimer = dzr_cld_CFG.GetFile(m_StoreFolder, SteamID+".txt", m_NullArray, m_Prisoners.Get( SteamID ).ToString(), IO_Command.GET, "Both", 0 );
            Print("m_Prisoners.Get( player ):" + m_Prisoners.Get( SteamID ));
            m_Prisoners.Remove( SteamID );
            Print("StoredTimer:" + StoredTimer);
            Print("■■■ SteamID: " + m_StoreFolder+"/"+SteamID);
            Print(m_Prisoners);
        }
    }
    
    void ReleasePrisoner(PlayerBase player)
    {
        Print("Trying to release");
        if(player)
        {
            string SteamID = player.GetIdentity().GetPlainId();
            if(m_Prisoners.Contains( SteamID ) )
            {
                m_Prisoners.Remove( SteamID );
            }
            //delete file
            //dzr_cld_CFG.GetFile(m_StoreFolder, SteamID+".txt", m_NullArray, "0", IO_Command.DELETE);
            DeleteFile(m_StoreFolder+"/"+SteamID+".txt");
            //teleport to coords
            if(!FileExist(m_StoreFolder+"/"+SteamID+".txt"))
            {
                if(m_CLDexistingConfig.TeleportOutAfterPrisonTimerEnds)
                {
                    player.SetPosition( m_CLDexistingConfig.ReleaseTeleportCoords.ToVector() );
                }
                //message
                Print("Released!");
                string theMessage = "Emprisonment time ended";
                
                NotificationSystem.SendNotificationToPlayerIdentityExtended( player.GetIdentity() , 10,  "You're free", theMessage, m_CLDexistingConfig.PlayerCombatWarningIcon);
            }
            else
            {
                Print("ERROR: Could not release "+SteamID);
            }
        }
    }
    
    void SERVER_SendToDiscord(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
    {
        //GetDayZGame().Debug("[DZR Combat Log Detection] ::: CLD_Player_Triggered");
        ref Param2<string, string> data;
        if ( !ctx.Read( data ) )
        {
            return;	
        }
        
        
        if (type == CallType.Server)
        {
            if (data.param1)
            {
                sendToDiscord(data.param1, data.param2);
                
            }
        }
    }
    
    
    void sendToDiscord(string message, string webhookURL)
    {
        if(m_CLDexistingConfig.NotifyOnDiscord)
        {
            // Compose the message to be sent to Discord.
            CLDDiscordMessage m_CLDDiscordMessage = new CLDDiscordMessage();
            m_CLDDiscordMessage.username = "Combat Log Detection";
            m_CLDDiscordMessage.content = message;
            m_CLDDiscordMessage.avatar = "https://cdn.discordapp.com/attachments/948492094564089906/948848483157307432/unknown.png";
            
            // Convert that message into JSON format.
            string discordMessageJSON;
            JsonSerializer jsonSerializer = new JsonSerializer();
            jsonSerializer.WriteToString(m_CLDDiscordMessage, false, discordMessageJSON);
            //https://cdn.discordapp.com/attachments/948492094564089906/948848483157307432/unknown.png
            // Post the message to Discord.
            private RestApi restAPI;
            private RestContext restContext;
            webhookURL.Replace("https://discord.com/api/webhooks/", "");
            
            if(GetGame().IsServer())
            {
                restAPI = CreateRestApi();
                restContext = restAPI.GetRestContext("https://discord.com/api/webhooks/");
                restContext.SetHeader("application/json");
                restContext.POST(NULL, webhookURL, discordMessageJSON);
                
                //GetDayZGame().Debug("m_CLDexistingConfig.NotifyOnDiscord: " + m_CLDDiscordMessage.content);
            }
        }
    }
    
    override void OnClientDisconnectedEvent(PlayerIdentity identity, PlayerBase player, int logoutTime, bool authFailed)
    {
        player.SetPrisonTimerRunning(false);
        StopPrisonTimer(player, identity.GetPlainId());
        
        if ( player.GetKickOffReason() == EClientKicked.SERVER_EXIT || player.GetKickOffReason() == EClientKicked.KICK_ALL_ADMIN || player.GetKickOffReason() == EClientKicked.KICK_ALL_SERVER || player.GetKickOffReason() == EClientKicked.SERVER_SHUTDOWN )
        {
            //ignore penalty
        }
        else 
        {
            
            //NEAREST OBJECTS
            
            ref array<Object> nearest_objects = new array<Object>;
            ref array<CargoBase> proxy_cargos = new array<CargoBase>;
            vector pos = player.GetPosition();
            
            
            float calcDistance = m_CLDexistingConfig.CombatCheckRadius; 
            
            GetGame().GetObjectsAtPosition3D( pos, calcDistance, nearest_objects, proxy_cargos );
            
            
            for ( int i = 0; i < nearest_objects.Count(); ++i )
            {
                Object object = nearest_objects.Get( i );
                checkPlayers(object, player);
                //GetDayZGame().Debug("In range: "+object, 0, m_CLDexistingConfig.EnableDebug);
            }
            //GetDayZGame().Debug("--------------- ITEMS: "+i, 0, m_CLDexistingConfig.EnableDebug);
            
            
            if(player.isInCombat() && m_CLDexistingConfig.PenaltyTimer >= 1 && !isWaitingForPenalty(player, "OnClientDisconnectedEvent") ) // || player.IsBleeding())
            {
                //if(m_CLDexistingConfig.CldDisconnectAction == "kill")
                //logoutTime = logoutTime + m_CLDexistingConfig.PenaltyTimer + m_CLDexistingConfig.CombatModeCooldownTime_s );
                logoutTime = logoutTime + m_CLDexistingConfig.PenaltyTimer;
                string warningTitle = "#STR_COMBAT_WARNING";
                string warningText = "#STR_COMBAT_WARNING_TEXT";
                
                if(m_CLDexistingConfig.PlayerCombatWarningTitle != "")
                {
                    warningTitle = m_CLDexistingConfig.PlayerCombatWarningTitle;
                };
                if(m_CLDexistingConfig.PlayerCombatWarningText != "")
                {
                    warningText = m_CLDexistingConfig.PlayerCombatWarningText;
                };
                
                string theMessage = ":hourglass_flowing_sand: Logout timer in Combat Mode is started for player "+identity.GetName()+ " ("+identity.GetPlainId()+"). **Admins, please note**: If the player hasn't crashed the game, the player can still cancel the logout and get back to game without Combat Logging. Logout timer penalty: "+m_CLDexistingConfig.PenaltyTimer+"s";
                
                NotificationSystem.SendNotificationToPlayerIdentityExtended( identity , m_CLDexistingConfig.CombatModeCooldownTime_s, warningTitle, warningText, m_CLDexistingConfig.PlayerCombatWarningIcon);
                /*
                    Param1<string> Msg = new Param1<string>( theMessage );
                    GetGame().RPCSingleParam( player, ERPCs.RPC_USER_ACTION_MESSAGE, Msg, true, player.GetIdentity() );
                */
                GetDayZGame().Debug(identity.GetName()+ " ("+identity.GetPlainId()+" is in logout for "+logoutTime, 1, m_CLDexistingConfig.EnableDebug);
                /*
                    if(m_CLDexistingConfig.NotifyOnDiscord)
                    {
                    sendToDiscord(theMessage, m_CLDexistingConfig.DiscordUrl);
                    }
                */
            }
            
            
            
            
            //Penalty
            if( player && player.isInCombat() && player.IsPlayer() )
            {
                
                
                if( !isWaitingForPenalty(player, "OnClientDisconnectedEvent") )
                {
                    GetDayZGame().Debug(" "+player+" not in the waiting list, adding delayed penalties",2, m_CLDexistingConfig.EnableDebug);
                    //GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(this.DelayedPenalties);
                    if( !m_WaitingForPenalty.Contains(PlayerBase.Cast(player)) )
                    {
                        m_WaitingForPenalty.Insert(PlayerBase.Cast(player), true);
                    }
                    ///m_CLD.DelayedPenalties( identity,  player,  authFailed,  identity.GetName(),  identity.GetPlainId(),  logoutTime, m_CLDexistingConfig);
                }
            }
            
        }
        /*
            if (GetHive() && !authFailed)
            {			
            if (player.IsAlive())
            {	
            if (!m_LogoutPlayers.Contains(player) && !m_NewLogoutPlayers.Contains(player))
            {
            // allow reconnecting to old char only if not in cars, od ladders etc. as they cannot be properly synchronized for reconnect
            if (!player.GetCommand_Vehicle() && !player.GetCommand_Ladder())
            {
            GetGame().AddToReconnectCache(identity);
            }
            }
            
            }		
            }
        */
        super.OnClientDisconnectedEvent(identity, player, logoutTime, authFailed);
    };
    
    bool isWaitingForPenalty(PlayerBase player = NULL, string source = "")
    {
        PlayerBase listPlayer;
        GetDayZGame().Debug("m_WaitingForPenalty LIST "+source, 1, m_CLDexistingConfig.EnableDebug);
        
        for ( int i3 = 0; i3 < m_WaitingForPenalty.Count(); i3++ )
        {
            listPlayer = m_LogoutPlayers.GetKey(i3);
            if(listPlayer && listPlayer.IsPlayer())
            {
                if(listPlayer && listPlayer.IsAlive() )
                {
                    if(listPlayer.GetIdentity())
                    {
                        GetDayZGame().Debug("m_WaitingForPenalty :"+listPlayer.GetIdentity().GetName() + "("+listPlayer.GetIdentity().GetPlainId()+")", 0, m_CLDexistingConfig.EnableDebug);
                    }
                    else
                    {
                        GetDayZGame().Debug("m_WaitingForPenalty : [NO IDENTITY]"+listPlayer, 0, m_CLDexistingConfig.EnableDebug);
                    }
                }
                else
                {
                    GetDayZGame().Debug("m_WaitingForPenalty : [DEAD?]"+listPlayer, 0, m_CLDexistingConfig.EnableDebug);
                }
            }
        }
        if(player && player.IsPlayer())
        {
            if(m_WaitingForPenalty.Contains(PlayerBase.Cast(player)) )
            {
                if(player.GetIdentity())
                {
                    GetDayZGame().Debug("Player already waiting for penalty :"+player.GetIdentity().GetName() + "("+player.GetIdentity().GetPlainId()+")", 2, m_CLDexistingConfig.EnableDebug);
                }
                else
                {
                    GetDayZGame().Debug("Player already waiting for penalty :"+player+ "[NO IDENTITY]", 2, m_CLDexistingConfig.EnableDebug);
                }
                return true;
            }
            
            GetDayZGame().Debug("Player "+player+ " not found", 1, m_CLDexistingConfig.EnableDebug);
        }
        GetDayZGame().Debug("■■■■■■■■■■■■■■■■■■■■■■■■■■■isWaitingForPenalty false■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■", 1, m_CLDexistingConfig.EnableDebug);
        return false;
    }
    
    void DelayedPenalties(PlayerIdentity identity, PlayerBase player, bool authFailed, string name, string plainid, int logout)
    {
        
        //Penalty
        
        GetDayZGame().Debug("DelayedPenalties start ", 2, m_CLDexistingConfig.EnableDebug);
        
        if (GetHive() && !authFailed)
        {			
            if ( player.IsAlive() && player.IsPlayer()  && isWaitingForPenalty(player, "DelayedPenalties") )
            {	
                
                //void DelayedPenalties(PlayerIdentity identity, PlayerBase player, bool authFailed, string name, string plainid, int logout, bool getHive, ref map<PlayerBase, bool> m_EXTWaitingForPenalty,  bool instantPenalty2 = false)
                
                
                GetDayZGame().Debug("TRYING m_CLD.DelayedPenalties in 4s ", 2, m_CLDexistingConfig.EnableDebug);
                GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(m_CLD.DelayedPenalties, 4, false, identity,  player,  authFailed,  name,  plainid,  logout, m_CLDexistingConfig);
                //CheckIfPrisoner(player, plainid);
                //m_CLD.DelayedPenalties( identity,  player,  authFailed,  name,  plainid,  logout, m_CLDexistingConfig);
                //m_dzrCLD.DelayedPenalties( identity,  player,  authFailed,  name,  plainid,  logout, m_CLDexistingConfig);
                if ( m_WaitingForPenalty.Contains(PlayerBase.Cast(player)) )
                {
                    m_WaitingForPenalty.Remove(PlayerBase.Cast(player));
                };
                
                isWaitingForPenalty(player, "DelayedPenalties END");
            }
        }
        
        //Print("authFailed2: "+authFailed);
    }
    
    void remoteCanExitEarly(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
    {
        
        
        GetDayZGame().Debug(":::::::::::::::: Player pressed Logout Button", 0, m_CLDexistingConfig.EnableDebug);
        //this.Debug( "m_bLogoutNow SRV: " + GetGame().IsServer().ToString() + " CNT: " + GetGame().IsClient().ToString());
        string ExtraText = "";
        ref Param1<PlayerBase> data;
        if ( !ctx.Read( data ) )
        {
            GetDayZGame().Debug("data.param1 :"+data.param1, 0, m_CLDexistingConfig.EnableDebug);
            return;	
        }
        GetDayZGame().Debug("data.param1 :"+data.param1, 0, m_CLDexistingConfig.EnableDebug);
        GetDayZGame().Debug("Player received. Server satarts safe logout.", 0, m_CLDexistingConfig.EnableDebug);
        if (type == CallType.Server)
        {
            if (data.param1 != NULL)
            {
                GetDayZGame().Debug("data.param1: "+data.param1, 0, m_CLDexistingConfig.EnableDebug);
                if(m_CLDexistingConfig.ExitButtonTrigger)
                {
                    GetDayZGame().Debug("■■■■■ Sorry "+data.param1+". Admin decided to punish even for legit disconnect.", 2, m_CLDexistingConfig.EnableDebug);
                    ExtraText = " Penalties will be applied.";
                    m_CLD.DelayedPenalties( data.param1.GetIdentity(),  data.param1,  false,  data.param1.GetIdentity().GetName(),  data.param1.GetIdentity().GetPlainId(),  4, m_CLDexistingConfig);
                    data.param1.canExitEarly("LogoutMenu", false, true);
                    //CheckIfPrisoner(data.param1, data.param1.GetIdentity().GetPlainId());
                } 
                else
                {
                    data.param1.canExitEarly("LogoutMenu", true);
                };
                
                //if( !WaitingForLogout.Contains(data.param1) )
                //{
                WaitingForLogout.Insert(data.param1);
                //};
                
                if(m_CLDexistingConfig.NotifyOnDiscord)
                {
                    if(m_CLDexistingConfig.ExitButtonNotify)
                    {
                        sendToDiscord(":door: Player "+data.param1.GetIdentity().GetName()+" ("+data.param1.GetIdentity().GetPlainId()+") pressed the EXIT button in Combat Mode."+ExtraText , m_CLDexistingConfig.DiscordUrl);
                    };
                }
                
            }
        }
        
    }
    
    
    
    //void instantPenalty(PlayerBase player)
    void instantPenalty(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
    {
        GetDayZGame().Debug("Someone wants instant penalty. Checking.", 0, m_CLDexistingConfig.EnableDebug);
        if(m_CLDexistingConfig.KillInstantPenalty)
        {
            GetDayZGame().Debug("m_CLDexistingConfig.KillInstantPenalty: "+m_CLDexistingConfig.KillInstantPenalty, 0, m_CLDexistingConfig.EnableDebug);
            ref Param1<PlayerBase> data;
            if ( !ctx.Read( data ) )
            {
                GetDayZGame().Debug("NO!!! data.param1 :"+data.param1, 0, m_CLDexistingConfig.EnableDebug);
                return;	
            }
            //this.Debug("data.param1 :"+data.param1);
            //this.Debug("Player received. Server satarts safe logout.");
            if (type == CallType.Server)
            {
                if (data.param1 != NULL)
                {
                    
                    PlayerBase player = data.param1;
                    PlayerIdentity identity = data.param1.GetIdentity();
                    string SteamID = data.param1.GetIdentity().GetPlainId();
                    
                    StartPrisonTimer(player, SteamID);
                    
                    GetDayZGame().Debug("Instant Penalty Activated", 0, m_CLDexistingConfig.EnableDebug);
                    GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(this.DelayedPenalties, 0, false, identity, player, false, identity.GetName(), identity.GetPlainId(), 0, true);
                    
                    
                }
            }
        }
    }	
    
    void removePenalty(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
    {
        GetDayZGame().Debug("Received RPC to remove delayed penalty", 2, m_CLDexistingConfig.EnableDebug);
        ref Param1<PlayerBase> data;
        if ( !ctx.Read( data ) )
        {
            //this.Debug("data.param1 :"+data.param1);
            return;	
        }
        GetDayZGame().Debug("data.param1 :"+data.param1, 0, m_CLDexistingConfig.EnableDebug);
        //this.Debug("Player received. Server satarts safe logout.");
        if (type == CallType.Server)
        {
            if (data.param1 != NULL)
            {
                GetDayZGame().Debug("m_WaitingForPenalty cleanup", 2, m_CLDexistingConfig.EnableDebug);
                if ( m_WaitingForPenalty.Contains(PlayerBase.Cast(data.param1)) )
                {
                    m_WaitingForPenalty.Remove(PlayerBase.Cast(data.param1));
                };
                
                if ( m_WaitingForPenalty.Contains(PlayerBase.Cast(data.param1)) )
                {
                    m_WaitingForPenalty.Remove(PlayerBase.Cast(data.param1));
                };
                
                if ( m_WaitingForPenalty.Contains(PlayerBase.Cast(data.param1)) )
                {
                    m_WaitingForPenalty.Remove(PlayerBase.Cast(data.param1));
                };
                
                //m_WaitingForPenalty.Remove(PlayerBase.Cast(data.param1));
                //GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(this.DelayedPenalties);
                
                /*
                    if(m_CLDexistingConfig.NotifyOnDiscord)
                    {
                    sendToDiscord(":white_check_mark: Player "+data.param1.GetIdentity().GetName()+" ("+data.param1.GetIdentity().GetPlainId()+") is back to game. Logout cancelled." , m_CLDexistingConfig.DiscordUrl);
                    }
                */
            }
        }
        
    }	
    
    void usedAltF4(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender)
    {
        ref Param1<PlayerBase> data;
        if ( !ctx.Read( data ) ) return;
        if (type == CallType.Server)
        {
            if (data.param1 != NULL)
            {
                //PlayerBase player = PlayerBase.Cast( sender );
                if(data.param1)
                {
                    if(m_CLDexistingConfig.NotifyOnDiscord && data.param1.isInCombat())
                    {
                        sendToDiscord(":no_entry: Player "+sender.GetName()+" ("+sender.GetPlainId()+") used ALT+F4 in Combat Mode." , m_CLDexistingConfig.DiscordUrl);
                    }
                }
                
            }
        }
    }
    
    void checkPlayers (Object object, PlayerBase checker)
    {
        PlayerBase player = PlayerBase.Cast( object );
        if ( player && player != checker)
        {
            //GetDayZGame().Debug("Found players. Checking if in combat.");
            if( player.isInCombat() && player.IsAlive() )
            {
                GetDayZGame().Debug("Combat Player around: "+player.GetName(), 1, m_CLDexistingConfig.EnableDebug);
                checker.setCombatMode(true, "Combat Player around: "+player.GetName());
                checker.resetCombatTimer();	
            }
        }
        else 
        {
            //GetDayZGame().Debug("No players around");
        }
        
        if(m_CLDexistingConfig.InfectedTrigger && !m_CLDexistingConfig.PlayerOnlyDamage)
        {
            ZombieBase theZombie = ZombieBase.Cast( object );
            if ( theZombie )
            {
                //GetDayZGame().Debug("Found a zombie. Checking if targets.");
                if( theZombie.getTarget(checker) )
                {
                    GetDayZGame().Debug("Player targeted by an infected", 1, m_CLDexistingConfig.EnableDebug);
                    checker.setCombatMode(true, "Player targeted by an infected");
                    checker.resetCombatTimer();	
                }
            }
        }
        
        if(m_CLDexistingConfig.PredatorTrigger && !m_CLDexistingConfig.PlayerOnlyDamage)
        {
            AnimalBase thePredator = AnimalBase.Cast( object );
            if ( thePredator )
            {
                if( thePredator.IsDanger() )
                {
                    GetDayZGame().Debug("Player is near a predator.", 1, m_CLDexistingConfig.EnableDebug);
                    checker.setCombatMode(true, "Player is near a predator.");
                    checker.resetCombatTimer();	
                }
            }
        }	
        
        if(m_CLDexistingConfig.BrokenLegsTrigger && !m_CLDexistingConfig.PlayerOnlyDamage)
        {
            if ( checker.GetBrokenLegs() != eBrokenLegs.NO_BROKEN_LEGS )
            {
                GetDayZGame().Debug("Player has broken legs.", 1, m_CLDexistingConfig.EnableDebug);
                checker.setCombatMode(true, "Player has broken legs.");
                checker.resetCombatTimer();	
                
            }
        }
        
        if(m_CLDexistingConfig.BloodlossTrigger && !m_CLDexistingConfig.PlayerOnlyDamage)
        {
            if ( checker.IsBleeding() )
            {
                GetDayZGame().Debug("Player is bleeding.", 1, m_CLDexistingConfig.EnableDebug);
                checker.setCombatMode(true, "Player is bleeding.");
                checker.resetCombatTimer();	
                
            }
        }
        
        #ifdef NAMALSK_SURVIVAL
            if(m_CLDexistingConfig.NamalskEVRStormTrigger){
                
                if(!checker.m_Environment.IsSafeFromEVR())
                {
                    checker.setCombatMode(true, "Player is in Namalsk storm");
                    checker.resetCombatTimer();	
                }
            }
            
            if(m_CLDexistingConfig.NamalskBlizzardTrigger){
                
                NamEventBase event_bliz = NamEventManager.GetInstance().GetEvent( Blizzard );
                
                
                if(event_bliz)
                {
                    checker.setCombatMode(true, "Player is in Blizzard");
                    checker.resetCombatTimer();	
                }
            }
        #endif
        
        
    }
    
    override void PlayerDisconnected(PlayerBase player, PlayerIdentity identity, string uid)
    {
        //GetDayZGame().Debug("Player disconnected: "+uid +" Player alive? : "+player.IsAlive() + " player.isInCombat(): "+player.isInCombat());
        /*
            if( player.isInCombat()) 
            {
            float currentHealth = player.GetHealth("","");
            player.SetHealth("", "", currentHealth - m_CLDexistingConfig.PenaltyHealth);
            string extraText;
            
            if(m_CLDexistingConfig.NotifyOnDiscord)
            {
            if(currentHealth <= m_CLDexistingConfig.PenaltyHealth)
            {
            extraText = " Current health: "+currentHealth+". Character will probably die.";
            }
            sendToDiscord(uid+" disconnected while in Combat Mode. received -"+m_CLDexistingConfig.PenaltyHealth+" health penalty."+extraText, m_CLDexistingConfig.DiscordUrl);
            }
            }
        */
        super.PlayerDisconnected( player,  identity,  uid);
    }
    
    
    
    bool DZR_CLD_MissionServer_ReadConfigFile()
    {
        //GetDayZGame().Debug("[DZR Combat Log Detection] Server ::: ACTIVE ");
        //PROPER CONFIG
        /*
            const static string dzr_Cld_ProfileFolder = "$profile:\\";
            const static string dzr_Cld_TagFolder = "DZR\\";
            const static string dzr_Cld_ModFolder = "IdentityZ\\";
        */
        //GetDayZGame().Debug("[DZR Combat Log Detection] ::: DZR_MissionServer_ReadConfigFile");
        if (!FileExist(dzr_Cld_ProfileFolder+dzr_Cld_TagFolder)){
            //NO DZR
            //GetDayZGame().Debug("[DZR Combat Log Detection] ::: DZR_MissionServer_ReadConfigFile NO DZR");
            MakeDirectory(dzr_Cld_ProfileFolder+dzr_Cld_TagFolder);	
            MakeDirectory(dzr_Cld_ProfileFolder+dzr_Cld_TagFolder+dzr_Cld_ModFolder);	
            return dzr_cld_writeConfigFile();			
        }
        else 
        {
            //YES DZR
            //GetDayZGame().Debug("[DZR Combat Log Detection] ::: DZR_MissionServer_ReadConfigFile YES DZR");
            if (!FileExist(dzr_Cld_ProfileFolder+dzr_Cld_TagFolder+dzr_Cld_ModFolder)){
                //NO IDZ
                //GetDayZGame().Debug("[DZR Combat Log Detection] ::: DZR_MissionServer_ReadConfigFile NO IDZ");
                MakeDirectory(dzr_Cld_ProfileFolder+dzr_Cld_TagFolder+dzr_Cld_ModFolder);
                return dzr_cld_writeConfigFile();
            }
            else 
            {
                //YES IDZ
                //GetDayZGame().Debug("[DZR Combat Log Detection] ::: DZR_MissionServer_ReadConfigFile YES IDZ");
                return dzr_cld_writeConfigFile();
            }
            
        };
        
    }
    
    bool dzr_cld_writeConfigFile()
    {
        //FILE HANDLING
        
        
        string m_configFilePath = dzr_Cld_ProfileFolder+dzr_Cld_TagFolder+dzr_Cld_ModFolder+dzr_Cld_ConfigFile;
        //GetDayZGame().Debug("[DZR Combat Log Detection] ::: DZR_MissionServer_ReadConfigFile");
        if (!FileExist(m_configFilePath)) 
        {
            //GetDayZGame().Debug("[DZR Combat Log Detection] ::: (!FileExist(dzr_Cld_ProfileFolder)) "+dzr_Cld_ProfileFolder);
            if (!m_CLDdefaultConfig) 
            {
                //GetDayZGame().Debug("[DZR Combat Log Detection] ::: (!m_CLDdefaultConfig)"+m_CLDdefaultConfig);
                m_CLDdefaultConfig = new dzr_cld_config_data_class();
                GetDayZGame().CldSaveConfigOnServer(m_CLDdefaultConfig);
                JsonFileLoader<dzr_cld_config_data_class>.JsonSaveFile(m_configFilePath, m_CLDdefaultConfig);
            }
            //GetDayZGame().Debug("[DZR Combat Log Detection] ::: +(!m_CLDdefaultConfig) "+m_CLDdefaultConfig);
            GetDayZGame().CldSaveConfigOnServer(m_CLDdefaultConfig);
            JsonFileLoader<dzr_cld_config_data_class>.JsonSaveFile(m_configFilePath, m_CLDdefaultConfig);
        } 
        else 
        {
            //GetDayZGame().Debug("[DZR Combat Log Detection] ::: +FileExist(dzr_Cld_ProfileFolder) "+dzr_Cld_ProfileFolder);
            JsonFileLoader<dzr_cld_config_data_class>.JsonLoadFile(m_configFilePath, m_CLDexistingConfig);
            m_CLDdefaultConfig = new dzr_cld_config_data_class();
            if (m_CLDdefaultConfig.ConfigVersion != m_CLDexistingConfig.ConfigVersion)
            {
                int hour;
                int minute;
                int second;
                int year;
                int month;
                int day;
                GetHourMinuteSecond(hour, minute, second);
                GetYearMonthDay(year, month, day);
                string suffix = year.ToString()+"-"+month.ToString()+"-"+day.ToString()+"_"+hour.ToString()+"."+minute.ToString()+"."+second.ToString();
                JsonFileLoader<dzr_cld_config_data_class>.JsonSaveFile(m_configFilePath+"_BACKUP_"+suffix+".txt", m_CLDexistingConfig);
                GetDayZGame().CldSaveConfigOnServer(m_CLDdefaultConfig);
                JsonFileLoader<dzr_cld_config_data_class>.JsonSaveFile(m_configFilePath, m_CLDdefaultConfig);
            }
            else 
            {
                JsonFileLoader<dzr_cld_config_data_class>.JsonLoadFile(m_configFilePath, m_CLDexistingConfig);
            }
            GetDayZGame().CldSaveConfigOnServer(m_CLDexistingConfig);
            //JsonFileLoader<dzr_cld_config_data_class>.JsonSaveFile(m_configFilePath, m_CLDexistingConfig);
        }
        
        
        return true;
    }
    
    
    //FILE HANDLING
    
    /*
        override void HandleBody(PlayerBase player)
        {
        PlayerIdentity identity = player.GetIdentity();
        PlayerIdentity uid = identity.GetPlainId()
        PlayerIdentity name = identity.GetName()
        if(player)
        {
        if( player.isInCombat()) 
        {
        player.SetHealth("", "", player.GetHealth("","") - m_CLDexistingConfig.PenaltyHealth);
        
        if(m_CLDexistingConfig.EnableDebug)
        {
        if(m_CLDexistingConfig.NotifyOnDiscord)
        {
        if(player.GetIdentity())
        {
        sendToDiscord(player.GetIdentity().GetName()+ " ("+player.GetIdentity().GetPlainId()+") disconnected while in Combat Mode. received -"+m_CLDexistingConfig.PenaltyHealth+" health penalty. .Current health: "+player.GetHealth(), m_CLDexistingConfig.DiscordUrl);
        }
        }
        }
        }
        }
        super.HandleBody( player);
        }
    */
    void Debug(string strDebugMessage, int priority = 0, bool Enabled = false)
    {
        if(Enabled){
            string m_modName = "[DZR Combat Log Detection missionServer.c]";
            string m_side = "NULL";
            
            string delimiter = " ::::::::: ";
            
            if(priority == 1)
            {
                delimiter = " ■ ■ ■ ■ ■ ";	
            }
            if(priority == 2)
            {
                delimiter = " ■■■■■■■■■ ";
            }
            
            PlayerBase theClient = PlayerBase.Cast( GetGame().GetPlayer() );
            
            if(GetGame().IsClient())
            {
                m_side = "Clientside";
            };
            
            if(GetGame().IsServer())
            {
                m_side = "Serverside";
            };
            
            int hour;
            int minute;
            int second;
            
            GetHourMinuteSecond(hour, minute, second);
            //GetYearMonthDay(year, month, day);
            string suffix = hour.ToString()+":"+minute.ToString()+":"+second.ToString();
            
            
            if(theClient && theClient.GetIdentity())
            {
                GetGame().Chat(m_modName+delimiter+strDebugMessage, "colorImportant");
                Print("■"+suffix+"■ "+m_modName+delimiter+strDebugMessage+" ::: "+m_side);
                Param1<string> Msg = new Param1<string>( strDebugMessage );
                GetGame().RPCSingleParam( theClient, ERPCs.RPC_USER_ACTION_MESSAGE, Msg, true, theClient.GetIdentity() );
            }
        }
    }
    //Lock doors
    void LockDoors(vector position, int radius)
    {    
        
        
        vector pos = position;
        ref array<Object> nearest_objects = new array<Object>;
        ref array<CargoBase> proxy_cargos = new array<CargoBase>;
        GetGame().GetObjectsAtPosition ( pos, radius, nearest_objects, proxy_cargos );
        
        for ( int i = 0; i < nearest_objects.Count(); i++ )
        {
            Object nearest_object = nearest_objects.Get(i);
            
            if ( nearest_object.IsInherited( Building ) )
            {    
                
                Building building = Building.Cast( nearest_object );
                int doorsCount = building.GetGame().ConfigGetChildrenCount("CfgVehicles " + building.GetType() + " Doors");                
                for (int c = 0; c < doorsCount; c++)
                {
                    //vector distanceRoot = vector.DistanceSq(player.GetPosition(), playerRootPos);
                    //if( this.IsInReach(player, target, radius) && !building.IsDoorOpen(c) ) 
                    //{
                    if ( building.IsDoorOpen(c) ) building.CloseDoor(c);
                    building.LockDoor(c);
                    //}
                    
                    
                }
            }    
        }
    }
    //Lock doors
    
    
}	

/*
    TESTING CHECKLIST:
    1) Shot
    2) 2 player Kill cycle
    3) Agro Zombie
    4) Cancel logout
    5) Kill both grenade
*/
#ifdef JM_COT
    modded class JMPlayerModule
    {
        
        override private void Exec_Kick( array< string > guids, PlayerIdentity ident, JMPlayerInstance instance = NULL, string messageText = ""  )
        {
            array< JMPlayerInstance > dzr_players = GetPermissionsManager().GetPlayers( guids );
            
            foreach (auto player: dzr_players)
            {
                
                player.setCombatMode(false);
                player.SetPrisonTimerRunning(false);
                if (!player.PlayerObject)
                continue;
                
                
            }
            
            super.Exec_Kick(guids, ident, instance = NULL, messageText = "");
        }
        
    };
#endif


